class Sensor(object):

    def __init__(self):

        self._temperature = 0    #instance attribute.
        self._humidity = 0       #instance attribute.
        self._luminance = 0      #instance attribute.


    @property
    def temperature(self):
        return self._temperature
    @temperature.setter
    def temperature(self, value):
        self._temperature = value
        
    @property
    def humidity(self):
        return self._humidity
    @humidity.setter
    def humidity(self, value): 
        self._humidity = value
    
    @property
    def luminance(self):
        return self._luminance
    @luminance.setter
    def luminance(self, value):
        self._luminance = value

