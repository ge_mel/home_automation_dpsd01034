#!/usr/bin/python
# -*- coding: utf-8 -*- 
#
#  simple app to read string from serial port 
#  and manipulate it in order to create subtopics
#  for publishing via MQTT to openHAB
#
#  uses the paho-mqtt 1.1 client MQTT version 3.1/3.1.1 client class
#  https://pypi.python.org/pypi/paho-mqtt
#  
#  George Melampianakis ( Andy Piper's code improvement for my current raspberryPi moteino serial[UART] RFgateway setup [https://gist.github.com/andypiper/1218932] )
#  2016/01/06
#
#  Message Manipulation
#  Message structure: N:2&T:21&H:35L:300 , NODE:ID&Temperature:value&Humidity:value&Luminance:value
#  Each message refers to a different topic. The topic is a path formatted like, 
#                   home/rfm_gw/direction/nodeID/deviceID
#  eg. home/rfm_gw/sb/node02/devT, where node02 is the 1st sensor item (node01 is the gateway on raspberryPi) & devT is device Temperature

from __future__ import print_function   # item separators, EOL character (in case the end of readline character is needed / EOL )
from classes.sensor import Sensor       # Sensory class for storing & comparing the values of each sensor object. If a device value has change then publish on MQTT for the respective device
import os                               # (might be exaggeration)
import serial                           # enable serial port
import paho.mqtt.client as mqtt         # Paho MQTT library
import datetime                         # time and date functions (in case the messages with time and date are going to be written in a log )
import time                             # is needed for delays
import sys                              # debugging purposes for checking sizes of the objects - might be exaggeration
import re                               # regular expressions


topic = "home/rfm_gw/nb/"       # for now all messages are northbound : only read sensory (!)
serialdev = "/dev/ttyS0"        # serial port to use    // /dev/spidev0.0
broker = "localhost"            # 127.0.0.1 OR localhost
port = 1883                     # port
mycounter = 0                   # debugging purposes - get the count of serial or MQTT loop
sensors = []                    # sensory id list
sensorCounter = 0               # THis counter should reflect the number of sensors of the sensor network
currentSensor = None
debug = True                   # Enable/disable terminal debug 

# MQTT callbacks
def on_connect(mosq, obj, rc):
    if rc == 0:
    # on connect should create topic in mosquitto
    # rc 0 successful connect
        if debug == True:
            print ("Connected")
    else:
        raise Exception

def on_publish(mosq, obj, mid):
    if debug == True:
        print ("Published ", str(mid))
    pass

# Paho MQTT Custom function
# Below the device refers to the actual sensory item (eg. temperature, humidity)
def publish_subtopics(nodeid, device, value):
    temptopic = "%snode%s/dev%s" % (topic , nodeid, device)
    mqttc.publish( temptopic, int(value) )

# called on exit
# close serial, disconnect MQTT
def cleanup():
    if debug == True:
        print ("Ending and cleaning up")
    ser.close()
    mqttc.disconnect()

try:
    if debug == True:
        print ("Connecting... ", serialdev)
    # connect to serial port
    ser = serial.Serial( # Serial properties
      port = serialdev,
      baudrate = 115200,
      #parity = serial.PARITY_ONE,
      #stopbits = serial.STOPBITS_ONE,
      bytesize = serial.EIGHTBITS,
      timeout = None
    )
    ser.close()

except:
    if debug == True:
        print ("Failed to connect serial")
    # unable to continue with no serial input
    raise SystemExit

ser.open()   # open the serial com port to receive data
while True:                          # loop forever, until process is killed
        dat = ser.readline()         # Read serial check. When serial data exist go for MQTT loop
        if debug == True:
            print ("Serial message stucture length: %d" % len(dat))
        if (len(dat) > 0):           # get serial message
          try:
              mypid = os.getpid()
              
              # create an mqtt client
              mqttc = mqtt.Client()

              # attach MQTT callbacks
              mqttc.on_connect = on_connect
              mqttc.on_publish = on_publish

              # connect to broker
              mqttc.connect(broker, port, 60)
              
              # remain connected to broker
              # read data from serial and publish
              while mqttc.loop() == 0:
                    
                    # Clean serial input
                    ser.flushInput()
                    # Read & clean serial message
                    rawline = ser.readline()
                    # Check if the serial message is valid [ the first character of the message is N ]
                    if rawline[:1] == 'N':
                        #stringline = str(rawline)
                        line = re.sub('[\s\x00\r\n]', '', rawline)
                        
                        # Check raw message (includes EOL,\x00,\r) 
                        if debug == True:
                            debugline = repr(rawline)
                            print(debugline)
                        
                        # Breakdown of the serial message
                        # DEVICES    
                        # DeviceT = line.split('&')[1].split(':')[0]  # Temperature
                        # DeviceH = line.split('&')[2].split(':')[0]  # Humidity
                        # DeviceL = line.split('&')[3].split(':')[0]  # Light Dependent Resistor
                        #
                        # Respective VALUES of devices above   
                        # Temperature = line.split('&')[1].split(':')[1] # tempValue
                        # Humidity = line.split('&')[2].split(':')[1] # humValue
                        # Ldr = line.split('&')[3].split(':')[1] # ldrValue
                        
                        Nodeid_ = int(line.split('&')[0].split(':')[1]) # Sensor node ID from current serial message
                        temp_ = int(line.split('&')[1].split(':')[1]) # Sensor temperature from current serial message
                        humi_ = int(line.split('&')[2].split(':')[1]) # Sensor humidity from current serial message
                        lumi_ = int(line.split('&')[3].split(':')[1]) # Sensor luminance from current serial message
                        
                        # check loop count
                        if debug == True:
                            print("MQTT loop count: %d" % mycounter) 
                            mycounter += 1
    
                        # Create Sensor objects
                        if Nodeid_ not in sensors:
                            sensors.append( Nodeid_ )
                            sensor = Sensor()
                            setattr(sensor, "id" , sensors[sensorCounter])
                            if debug == True:
                                print("sensor with id: %d registered" % sensor.id)
                            sensorCounter += 1
                        
                       # if currentSensor != Nodeid_:
                        currentSensor = Nodeid_
                        # Sensor to be checked for changes in MQTT loop
                        if debug == True:
                            print("current sensor id %d" % currentSensor)
                        
                        if sensor.temperature != temp_ and sensor.id == currentSensor:
                            sensor.temperature = temp_
                            publish_subtopics( sensor.id , line.split('&')[1].split(':')[0], sensor.temperature)
#                           temptopic = "%snode%s/dev%s" % (topic , currentSensor, line.split('&')[1].split(':')[0])
#                           mqttc.publish( temptopic, int(sensor.temperature) )
                            if debug == True:
                                print("new temperature value= %d, for sensor id: %d" % (sensor.temperature, sensor.id))
                            
                        if sensor.humidity != humi_ and sensor.id == currentSensor:
                            sensor.humidity = humi_
                            publish_subtopics( sensor.id , line.split('&')[2].split(':')[0], sensor.humidity)
#                           temptopic = "%snode%s/dev%s" % (topic , currentSensor, line.split('&')[2].split(':')[0])
#                           mqttc.publish( temptopic, int(sensor.humidity) )
                            if debug == True:
                                print("new humidity value= %d, for sensor id: %d" % (sensor.humidity, sensor.id))
                            
                        if sensor.luminance != lumi_ and sensor.id == currentSensor:
                            sensor.luminance = lumi_
                            publish_subtopics( sensor.id , line.split('&')[3].split(':')[0], sensor.luminance)
#                           temptopic = "%snode%s/dev%s" % (topic , currentSensor, line.split('&')[3].split(':')[0])
#                           mqttc.publish( temptopic, int(sensor.luminance) )
                            if debug == True:
                                print("new luminance value= %d, for sensor id: %d" % (sensor.luminance, sensor.id))
                    
                        # Reset Nodeid_ & clean serial output
                        #Nodeid_ = None
                        #currentSensor = None
                        ser.flushOutput()
#                     else:
#                         pass   
          # handle list index error (i.e. assume no data received)
          except (IndexError):
            if debug == True:
                print ("No data received within serial timeout period")
            cleanup()
          # handle app closure
          except (KeyboardInterrupt):
            if debug == True:
                print ("Interrupt received")
            cleanup()
          except (RuntimeError):
            if debug == True:
                print ("Serial communication down! time to die")
            cleanup()