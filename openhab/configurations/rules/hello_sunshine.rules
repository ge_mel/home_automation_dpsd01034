import org.openhab.core.library.types.*
import org.openhab.core.persistence.*
import org.openhab.model.script.actions.*
import org.openhab.io.multimedia.actions.Audio.*
import org.openhab.core.library.types.*
import java.lang.Math


var Number counter = 0
var Timer timer = null
//var Timer timer_lm_0 = null
var Timer timer_lm_1 = null
var Timer timer_lm_2 = null
var Timer timer_lm_3 = null
var Timer timer_lm_4 = null
var Timer timer_lm_5 = null
//var Timer timer_lm_6 = null
var Timer timer_lm_7 = null
//var Timer timer_lm_8 = null
//var Timer timer_lm_9 = null
//var Timer timer_lm_10 = null
//var temp = null
//var humi = null
//var lumin = null
var Number temp_counter = null
/*
 * Weather
 */
rule "Update max and min temperatures"
when
	Item Weather_Temperature changed or
	Time cron "0 0 0 * * ?" //or
	//System started
then	
	postUpdate(Weather_Temp_Max, Weather_Temperature.maximumSince(now.toDateMidnight).state)
	postUpdate(Weather_Temp_Min, Weather_Temperature.minimumSince(now.toDateMidnight).state)
end
// Creates an item that stores the last update time of this item
rule "Records last weather update time"
when
  Item Weather_Temperature received update
then
  postUpdate(Weather_LastUpdate, new DateTimeType())
end
// This rule will be used to test Scale transformation service
rule "Compute humidex"
when
    Item Weather_Temperature changed or
	Item Weather_Humidity changed
then
	var Number T = Weather_Temperature.state as DecimalType
	var Number H = Weather_Humidity.state as DecimalType	
	var Number x = 7.5 * T/(237.7 + T)
	var Number e = 6.112 * Math::pow(10, x.doubleValue) * H/100
	var Number humidex = T + (new Double(5) / new Double(9)) * (e - 10)
	postUpdate(Weather_Humidex, humidex)
end

/*
 * Lights
 */
rule "All Lights Toggle"
when
	Item Leds received command
then
	if(receivedCommand==ON) {
		if(timer==null) {
			// first ON command, so create a timer to turn the light off again
			timer = createTimer(now.plusSeconds(10)) [|
				Leds?.members.forEach(item|
					sendCommand(item, ON)
				)
			]
		} else {
			// subsequent ON command, so reschedule the existing timer
			timer.reschedule(now.plusSeconds(10))
		}
	} else if(receivedCommand==OFF) {
		// remove any previously scheduled timer
		if(timer!=null) {
			timer.cancel
			timer = null
		} else {
			timer = createTimer(now.plusSeconds(10)) [|
				Leds?.members.forEach(item|
					sendCommand(item, OFF)
				)
				postUpdate(Light_modes, 0)
			]
		}
	}
end
rule "Light Modes"
when
Item Light_modes received command 
then
	if (receivedCommand==0) {
		//Reset Bulbs
		sendCommand(Leds, OFF)
		logInfo("Lights Mode Updated", "Reset Bulbs | All bulbs OFF")
		//postUpdate(Leds, OFF)
	}
	if (receivedCommand==1) {
		//Night rest mode
		//sendCommand(Leds, OFF)
		if(timer_lm_1==null) {
			timer_lm_1 = createTimer(now.plusSeconds(10)) [|
				sendCommand(Light_AF_Kitchen_Ceiling_Nights, ON)
				postUpdate(Light_AF_Kitchen_Ceiling, ON)
				sendCommand(Light_AF_Bathroom_Nights, ON)
				postUpdate(Light_AF_Bathroom_Ceiling, ON)
				sendCommand(Light_AF_MainHall_Ceiling_dimm, 40)
				postUpdate(Light_AF_MainHall_Ceiling, ON)
			]
			logInfo("Lights Mode Updated", "Night rest mode ON")
		} else {
			// subsequent ON command, so reschedule the existing timer
			timer_lm_1.reschedule(now.plusSeconds(10))
		}
	}
	if (receivedCommand==2) {
		//Night operation mode
		//sendCommand(Leds, OFF)
		if(timer_lm_2==null) {
			timer_lm_2 = createTimer(now.plusSeconds(10)) [|
				sendCommand(Light_AF_Living_Ceiling_dimm, 80)
				postUpdate(Light_AF_Living_Ceiling, ON)
				sendCommand(Light_AF_Office_A_dimm, 80)
				postUpdate(Light_AF_Office_A, ON)
				sendCommand(Light_AF_Office_B_dimm, 80)
				postUpdate(Light_AF_Office_B, ON)
				sendCommand(Light_AF_MainHall_Ceiling_dimm, 80)
				postUpdate(Light_AF_MainHall_Ceiling, ON)
				sendCommand(Light_AF_Kitchen_Ceiling_dimm, 80)
				postUpdate(Light_AF_Kitchen_Ceiling, ON)
				sendCommand(Light_AF_Bathroom_dimm, 80)
				postUpdate(Light_AF_Bathroom_Ceiling, ON)
			]
			logInfo("Lights Mode Updated", "Night operation mode ON")
		} else {
			// subsequent ON command, so reschedule the existing timer
			logInfo("Lights Mode Updated", "Night operation mode - timer not null")
			timer_lm_2.reschedule(now.plusSeconds(10))
		}
	}
	if (receivedCommand==3) {
		//Night work mode
		//sendCommand(Leds, OFF)
		if(timer_lm_3==null) {
			// first ON command, so create a timer to turn the light off again
			timer_lm_3 = createTimer(now.plusSeconds(10)) [|
				sendCommand(Light_AF_Living_Ceiling_dimm, 10)
				postUpdate(Light_AF_Living_Ceiling, ON)
				sendCommand(Light_AF_Office_A_dimm, 100)
				postUpdate(Light_AF_Office_A, ON)
				sendCommand(Light_AF_Office_B_dimm, 100)
				postUpdate(Light_AF_Office_B, ON)
				sendCommand(Light_AF_MainHall_Ceiling_dimm, 50)
				postUpdate(Light_AF_MainHall_Ceiling, ON)
				sendCommand(Light_AF_Kitchen_Ceiling_dimm, 30)
				postUpdate(Light_AF_Kitchen_Ceiling, ON)
				sendCommand(Light_AF_Bathroom_dimm, 30)
				postUpdate(Light_AF_Bathroom_Ceiling, ON)
			]
			logInfo("Lights Mode Updated", "Night work mode")
		} else {
			// subsequent ON command, so reschedule the existing timer
			timer_lm_3.reschedule(now.plusSeconds(10))
		}
	}
	if (receivedCommand==4) {
		//Night relax mode
		//sendCommand(Leds, OFF)
		if(timer_lm_4==null) {
			timer_lm_4 = createTimer(now.plusSeconds(10)) [|
				sendCommand(Light_AF_Living_Ceiling_night, ON)
				postUpdate(Light_AF_Living_Ceiling, ON)
				sendCommand(Light_AF_Office_A_dimm, 10)
				postUpdate(Light_AF_Office_A, ON)
				sendCommand(Light_AF_Office_B_dimm, 10)
				//sendCommand(Light_AF_Office_B_RGB, new HSBType(new DecimalType(202),new PercentType(54),new PercentType(44)))
				postUpdate(Light_AF_Office_B, ON)
				sendCommand(Light_AF_MainHall_Ceiling_dimm, 10)
				postUpdate(Light_AF_MainHall_Ceiling, ON)
				sendCommand(Light_AF_Kitchen_Ceiling_dimm, 10)
				postUpdate(Light_AF_Kitchen_Ceiling, ON)
				sendCommand(Light_AF_Bathroom_dimm, 10)
				postUpdate(Light_AF_Bathroom_Ceiling, ON)
			]
			logInfo("Lights Mode Updated", "Night relax mode ON")
		} else {
			// subsequent ON command, so reschedule the existing timer
			timer_lm_4.reschedule(now.plusSeconds(10))
		}		
	}
	if (receivedCommand==5) {
		//Day lumin support mode
		//sendCommand(Leds, OFF) //Only when day starts
		if(timer_lm_5==null) {
			timer_lm_5 = createTimer(now.plusSeconds(10)) [|
				sendCommand(Light_AF_Living_Ceiling_dimm, 20)
				postUpdate(Light_AF_Living_Ceiling, ON)
				sendCommand(Light_AF_Office_A_dimm, 50)
				postUpdate(Light_AF_Office_A, ON)
				sendCommand(Light_AF_Office_B_dimm, 50)
				postUpdate(Light_AF_Office_B, ON)
				sendCommand(Light_AF_MainHall_Ceiling_dimm, 30)
				postUpdate(Light_AF_MainHall_Ceiling, ON)
				sendCommand(Light_AF_Kitchen_Ceiling_dimm, 30)
				postUpdate(Light_AF_Kitchen_Ceiling, ON)
				sendCommand(Light_AF_Bathroom_dimm, 30)
				postUpdate(Light_AF_Bathroom_Ceiling, ON)
			]
		logInfo("Lights Mode Updated", "Day Luminance support mode ON")
		} else {
			// subsequent ON command, so reschedule the existing timer
			timer_lm_5.reschedule(now.plusSeconds(10))
		}
	}
	if (receivedCommand==6) {
		//Day operation mode
		sendCommand(Leds, OFF)
		logInfo("Lights mode updated", "Day operation mode | All bulbs OFF")
	}
	if (receivedCommand==7) {
		//Day work mode
		sendCommand(Leds, OFF)
		if(timer_lm_7==null) {
			timer_lm_7 = createTimer(now.plusSeconds(10)) [|
				sendCommand(Light_AF_Office_A_dimm, 100)
				postUpdate(Light_AF_Office_A, ON)
				sendCommand(Light_AF_Office_B_dimm, 100)
				postUpdate(Light_AF_Office_B, ON)
			]
			logInfo("Lights mode updated", "Day work mode")
		} else {
			// subsequent ON command, so reschedule the existing timer
			timer_lm_7.reschedule(now.plusSeconds(10))
			logInfo("Lights Mode Updated", "timer_lm_7 not null - reschedule the existing timer")
		}
	}
	if (receivedCommand==8) {
		//Day relax mode
		logInfo("Lights mode updated", "Day relax mode ON")
	}
	if (receivedCommand==9) {
		//Users away mode
		sendCommand(Leds, OFF)
		logInfo("Lights mode updated", "Users away mode | All bulbs OFF")
	}
	if (receivedCommand==10) {
		//Emergency mode
		//sendCommand(Light_AF_Living_Ceiling_disco, 100)
		RGBs?.members.forEach[item|
			item?.sendCommand(100)
		]
		//sendCommand(Light_AF_Living_Ceiling_RGB, new HSBType(new DecimalType(0),new PercentType(0),new PercentType(100)))
		//sendCommand(Leds, ON)
	}
end

//rule "Handle RGBs"
//when
//	Item Whites received command ON
//then
//	//Leds?.members.filter(s | s.state == ON).forEach[item|
//		//Nights?.members.filter(s | s.state == ON).sendCommand(OFF)
//		//sendCommand(Leds?.members.filter(s | s.state == ON), OFF)
//		logInfo("RGB ON", "WHITE & NIGHT OFF")
//	//]
//end
rule "Daily Light Operation - Night Relax"
	when
		Time cron "0 0 22 ? * MON,TUE,WED,THU,FRI"
	then
	
	Light_modes.sendCommand(4)
end


rule "Handle Living room Light"
when
	Item Light_AF_Living_Ceiling received command
then
	if(receivedCommand==ON) {
		Light_AF_Living_Ceiling_night.postUpdate(OFF)
		Light_AF_Living_Ceiling_white.postUpdate(OFF)
		Light_AF_Living_Ceiling_disco_switch.postUpdate(OFF)
	} 
end

rule "Handle Living room White"
when
	Item Light_AF_Living_Ceiling_white received command
then
	if(receivedCommand==ON) {
		Light_AF_Living_Ceiling_night.postUpdate(OFF)
		Light_AF_Living_Ceiling.postUpdate(OFF)
		Light_AF_Living_Ceiling_disco_switch.postUpdate(OFF)
		}
//	} else if (receivedCommand == OFF){
//		if (Light_AF_Living_Ceiling_night == OFF){
//			Light_AF_Living_Ceiling.postUpdate(OFF)
//		} else {
//			Light_AF_Living_Ceiling.postUpdate(ON)
//		}
//	}
end

rule "Handle Living room Night"
when
	Item Light_AF_Living_Ceiling_night received command
then
	if(receivedCommand==ON) {
		Light_AF_Living_Ceiling_white.postUpdate(OFF)
		Light_AF_Living_Ceiling.postUpdate(OFF)
		Light_AF_Living_Ceiling_disco_switch.postUpdate(OFF)
	}
end

rule "Handle Living room Disco"
when
    Item Light_AF_Living_Ceiling_disco_switch received command
then
	if ( receivedCommand == ON ) {
		Light_AF_Living_Ceiling_night.postUpdate(OFF)
		Light_AF_Living_Ceiling_white.postUpdate(OFF)
		Light_AF_Living_Ceiling.postUpdate(ON)
		Light_AF_Living_Ceiling_disco.sendCommand(40)
	} else if ( receivedCommand == OFF ){
		sendCommand(Light_AF_Living_Ceiling_disco, 0)
		Light_AF_Living_Ceiling_night.postUpdate(OFF)
		Light_AF_Living_Ceiling_white.postUpdate(OFF)
		Light_AF_Living_Ceiling.sendCommand(OFF)
	}
end

/*
 * Astro binding
 */
rule "Sunrise in ON"
when 
	Item Sunrise_Event received update ON
then
	sendCommand(Light_modes, 5)
	logInfo("Sunrise is ON", "Day luminance support mode is ON")
	postUpdate(Sunset_Event, OFF)
end

rule "Sunset in ON"
when 
	Item Sunset_Event received update ON
then
	sendCommand(Light_modes, 3)
	logInfo("Sunset is ON", "Night work mode is ON")
	postUpdate(Sunrise_Event, OFF)
end

rule "Turn to work mode automatically every working day"
when
	Time cron "0 30 10 ? * MON,TUE,WED,THU,FRI"
then
	sendCommand(Light_modes, 7)
end

/*
 * Security System 
 */
rule "Fire detection"
when
	Time cron "5 * * * * ?" or
    Item TempSensor changed
then
    // check for any temperature sensors in the house that are very high
    if (TempSensor.members.filter(s | s.state >= 43).size > 0) {        
        if (Fire_Alarm.state == OFF) {
            logInfo("sensor.rules", "Temperature is higher than expected!")
            //Fire_Alarm.postUpdate(ON)
            sendCommand(Fire_Alarm, ON)
        }       
    }else{
    	if(Fire_Alarm.state == ON){
    		logInfo("sensor.rules", "Temperature is normal")
    		//Fire_Alarm.postUpdate(OFF)
    		sendCommand(Fire_Alarm, OFF)
    	}
    }
end

rule "Fire alarm"
when
    Item Fire_Alarm received command ON
then
    // turn on all lights
    sendCommand(Light_modes, 10)
    // send notifications
 	// sendNotification("neutraltoe@gmail.com", "Fire alarm has been activated!")
 	sendBroadcastNotification("Fire alarm has been activated!")
 	// send an email
    sendMail("neutraltoe@gmail.com", "OpenHab Security: Fire Alarm", "The fire alarm has been activated!")
end

rule "Theft detection"
when
	Time cron "5 * * * * ?" or
	Item LuminSensor changed
then
	if (LuminSensor.members.filter(s | s.state > 100).size > 0) {        
        if (Presence.state == OFF) {
            logInfo("sensor.rules", "Somebody is home while no one is present!")
            //Theft_Alarm.postUpdate(ON)
            sendCommand(Theft_Alarm, ON)
        }       
    }else{
    	if(Theft_Alarm.state == ON){
    		//Theft_Alarm.postUpdate(OFF)
    		logInfo("sensor.rules", "All secure")
    		sendCommand(Theft_Alarm, OFF)
    	} 
    	
    }
end

rule "Theft alarm"
when
    Item Theft_Alarm received command ON
then
    // turn on all lights
    sendCommand(Light_modes, 10)
    // send notifications
 	//sendNotification("neutraltoe@gmail.com", "Theft alarm has been activated!")
 	sendBroadcastNotification("Theft alarm has been activated!")
 	// send an email
    //sendMail("neutraltoe@gmail.com", "OpenHab Security: Theft Alarm", "Theft alarm has been activated!")
end

//Check Presence
rule "Periodically check presence"
when
    Time cron "0 */5 * * * ?"
then
        if (Presence.state == ON)
        {
                if(gMobiles.members.filter(s | s.state == ON).size == 0) {
                        logInfo("PresenceCheck", "No phone within reach, checking for flapping")
                        if(gMobiles.members.filter(s | s.changedSince(now.minusMinutes(5))).size == 0) {
                                logInfo("PresenceCheck", "Nobody is at home")
                                sendCommand(Presence, OFF)
                        }
                }
        }
        else
        {
                //For initialisation. If Presence is undefined or off, although it should be on.
                if(gMobiles.members.filter(s | s.state == ON).size > 0) {
                        sendCommand(Presence, ON)
                }
                else if (Presence.state == Undefined || Presence.state == Uninitialized) {
                        sendCommand(Presence, OFF)
                }
                else {
                		sendCommand(Presence, OFF)
                }
        }

end

rule "Coming home"
when
	Item gMobiles changed
then
    if (Presence.state != ON) {
            if(gMobiles.members.filter(s | s.state == ON).size > 0) {
                    logInfo("PresenceCheck", "Somebody is home")
                    sendCommand(Presence, ON)
            }
    }
end

/*
 * Avg calculations
 */
rule "Calculate indoors temperature AVG"  
when
  Time cron "0 */10 * * * ?" or
  Item TempSensor changed  
then  
  var temp = TempSensor.averageSince(now.minusMinutes(1))  
  sendCommand(TempAvg, temp)
  logInfo("AVG temperature", "Updated")
end

rule "Calculate indoors humidity AVG"  
when  
  Time cron "0 */10 * * * ?" or
  Item HumSensor changed  
then  
  var humi = HumSensor.averageSince(now.minusMinutes(1))  
  sendCommand(HumAvg, humi)
  logInfo("AVG humidity", "Updated")
end

rule "Calculate indoors luminance AVG"  
when  
  Time cron "0 */10 * * * ?" or
  Item LuminSensor changed  
then  
  var lumin = LuminSensor.averageSince(now.minusMinutes(1))  
  sendCommand(LumAvg, lumin)
  // postUpdate
  logInfo("AVG luminance", "Updated")
end


/*
 * System Initialization
 */
//Parse RF to MQTT with python
//rule "Start parsing RF"
//	when
//		System started
//	then			executeCommandLine("python@@/opt/openhab/configurations/scripts/readSerialPublishMQTT.py ")
//end
rule "Initialize lights"
	when
		System started
	then
		if(Sunrise_Event == ON){
			sendCommand(Light_modes, 6)
			logInfo("Day is ON", "System init light mode: Day operation mode")
		}else if(Sunset_Event == ON){
			sendCommand(Light_modes, 2)
			logInfo("Night is ON", "System init light mode: Night relax mode")
		}else{
			Dimmers?.members.forEach(item|
				item.sendCommand(10)
			)
			sendCommand(Light_modes, 4)
			logInfo("Astro not initialized yet", "System init: All bulbs are OFF")
		}
		sendCommand(Fire_Alarm, OFF)
		sendCommand(Theft_Alarm, OFF)
end