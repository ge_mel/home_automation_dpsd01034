// George Melampianakis, 14/12/2015, node sketch for my current home automation setup [serial RF Gateway on Raspberry Pi B+]
// RFM69 sender/node sketch, with ACK and optional encryption
// RFM69 Library by Felix Rusu - felix@lowpowerlab.com
// Message Structure, node:id&temp:tempvalue&hum:humidvalue&lumin:luminvalue&..
// N:2&T:20.00&H:39.00&L:20.00&V:123&RT:12&..
/*
* Includes Radio
*/
#include <RFM69.h>    //get it here: https://www.github.com/lowpowerlab/rfm69
#include <RFM69_ATC.h>//get it here: https://www.github.com/lowpowerlab/rfm69
/*
* Includes SPI
*/
#include <SPI.h>
/*
* Includes Sensory
*/
#include <dht11.h> /*DHT11*/
/*
* Node Properties
*/
#define DEBUG         true
#define NODEID        2    //unique for each node on same network
#define NETWORKID     xxx  //the same on all nodes that talk to each other
#define GATEWAYID     1
#define FREQUENCY     RF69_868MHZ
#define ENCRYPTKEY    "xxxxxxxxxxxxxxxx" //exactly the same 16 characters/bytes on all nodes!
#define IS_RFM69HW    //uncomment only for RFM69HW! Leave out if you have RFM69W!
#define ENABLE_ATC    //comment out this line to disable AUTO TRANSMISSION CONTROL
#define LED           9 // Moteinos have LEDs on D9
#define SERIAL_BAUD   115200
//Create DHT11 Object
dht11 DHT11;
#define DHT11PIN      4
/*
* Sensory network reminder
*/
// Node 1 = "Gateway"
// Node 2 = "Office"
// Node 3 = "Bedroom"
// Node 4 = "Bathroom"
// Node 5 = "Kitchen"
//...
/*
* Current node's sensory definition
*/
#define THINGNAME             "N"     // (20) give a name (currently this requires only one letter)
#define NC                    -1      // Value for "not currently"
#define HASTEMPERATURE        true    // True if this device has a temperature sensor (DHT11)
#define HASHUMIDITY           true    // True if this device has a humidity sensor (DHT11)
#define HASLDR                true    // True if this device has a LDR sensor (LDR)

//Temperature and Humidity vars definition
int TempValue;
int HumValue;
//LDR pin and var definition
int ldrPin = A0; // select the Analog input pin for ldr
int LdrValue; // variable to store the value coming from the sensor
int radioTEMP;
int TRANSMITPERIOD = 1000; //transmit a packet to gateway so often (in ms) 1000
byte sendSize=0;
char buff[19];
char data;

#ifdef ENABLE_ATC
  RFM69_ATC radio;
#else
  RFM69 radio;
#endif

void setup() {
  Serial.begin(SERIAL_BAUD);
  radio.initialize(FREQUENCY,NODEID,NETWORKID);
#ifdef IS_RFM69HW
  radio.setHighPower(); //uncomment only for RFM69HW!
#endif
  radio.encrypt(ENCRYPTKEY);
//Auto Transmission Control - dials down transmit power to save battery (-100 is the noise floor, -90 is still pretty good)
#ifdef ENABLE_ATC
  radio.enableAutoPower(-70);
#endif
  //radio.setFrequency(919000000); //set frequency to some custom frequency
  
#ifdef ENABLE_ATC
  Serial.println("RFM69_ATC Enabled (Auto Transmission Control)\n");
#endif
}

long lastPeriod = 0;
void loop() {
 
  //check for any received packets // !Might be usefull for southbound MQTT commands from the gateway 
  if (radio.receiveDone())
  {
    Serial.print('[');Serial.print(radio.SENDERID, DEC);Serial.print("] ");
    for (byte i = 0; i < radio.DATALEN; i++)
      Serial.print((char)radio.DATA[i]);
    Serial.print("   [RX_RSSI:");Serial.print(radio.RSSI);Serial.print("]");

    if (radio.ACKRequested())
    {
      radio.sendACK();
      Serial.print(" - ACK sent");
    }
    Blink(LED,3);
    Serial.println();
  }

  int currPeriod = millis()/TRANSMITPERIOD;
  if (currPeriod != lastPeriod)
  {
    lastPeriod=currPeriod;

    //Check DHT11 sensor
    int chk = DHT11.read(DHT11PIN);
    Serial.print("Read sensor: ");
    switch (chk)
    {
      case DHTLIB_OK: 
        Serial.println("OK"); 
        break;
      case DHTLIB_ERROR_CHECKSUM: 
        Serial.println("Checksum error"); 
        break;
      case DHTLIB_ERROR_TIMEOUT: 
        Serial.println("Time out error"); 
        break;
      default: 
        Serial.println("Unknown error"); 
        break;
   }


    // Collect data
    if(HASHUMIDITY) HumValue = (int) floor(DHT11.humidity); else HumValue = NC; //LDR HUMIDITY
    if(HASTEMPERATURE) TempValue = (int) floor(DHT11.temperature); else TempValue = NC;  //LDR TEMPERATURE
    if(HASLDR) LdrValue = analogRead(ldrPin); else LdrValue = NC; //LDR VALUE
    radioTEMP = radio.readTemperature();  
    
    data=sprintf (buff, "%s:%d&T:%d&H:%d&L:%d", THINGNAME, NODEID, TempValue, HumValue, LdrValue);
    
    // Print Values to Serial before send
    if(DEBUG) {
        Serial.print("DATA: ");Serial.print((const char*)(&buff));Serial.println();
        Serial.println();
    }
    
    // Transmit to Gateway
    if(DEBUG) Serial.print("Transmitting...");
    radio.send(GATEWAYID, (const char*)(buff), sizeof(buff),true);
    if(DEBUG) Serial.println("done.");

    sendSize = (sendSize + 1) % 31;
    Serial.println();
    Blink(LED,1);
  }
}

void Blink(byte PIN, int DELAY_MS)
{
  pinMode(PIN, OUTPUT);
  digitalWrite(PIN,HIGH);
  delay(DELAY_MS);
  digitalWrite(PIN,LOW);
}
