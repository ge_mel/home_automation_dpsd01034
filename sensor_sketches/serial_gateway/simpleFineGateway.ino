// Simple serial pass through program
// It initializes the RFM12B radio with optional encryption and passes through any valid messages to the serial port
// felix@lowpowerlab.com

#include <RFM69.h>    //get it here: https://www.github.com/lowpowerlab/rfm69
#include <RFM69_ATC.h>//get it here: https://www.github.com/lowpowerlab/rfm69
#include <SPI.h>

// You will need to initialize the radio by telling it what ID it has and what network it's on
// The NodeID takes values from 1-127, 0 is reserved for sending broadcast messages (send to all nodes)
// The Network ID takes values from 0-255
// By default the SPI-SS line used is D10 on Atmega328. You can change it by calling .SetCS(pin) where pin can be {8,9,10}
#define NODEID          1  //network ID used for this unit
#define NETWORKID       xxx  //the network ID we are on
#define SERIAL_BAUD     115200
#define FREQUENCY     RF69_868MHZ
#define IS_RFM69HW    //uncomment only for RFM69HW! Leave out if you have RFM69W!
#define LED           9 // Moteinos have LEDs on D9
#define FLASH_SS      8 // and FLASH SS on D8
#define LED_green     7 // Some indicator

//encryption is OPTIONAL
//to enable encryption you will need to:
// - provide a 16-byte encryption KEY (same on all nodes that talk encrypted)
// - to call .Encrypt(KEY) to start encrypting
// - to stop encrypting call .Encrypt(NULL)
#define ENCRYPTKEY  "xxxxxxxxxxxxxxxx" //KEY[]
#define ENABLE_ATC    //comment out this line to disable AUTO TRANSMISSION CONTROL
// Need an instance of the Radio Module
#ifdef ENABLE_ATC
  RFM69_ATC radio;
#else
  RFM69 radio;
#endif
bool promiscuousMode = true; //set to 'true' to sniff all packets on the same network

void setup()
{ radio.setCS(FLASH_SS);
  radio.initialize(FREQUENCY,NODEID,NETWORKID);
  radio.setHighPower(); //only for RFM69HW!
  radio.encrypt(ENCRYPTKEY);
  radio.promiscuous(promiscuousMode);
  char buff[50];
  delay(10);
  Serial.begin(SERIAL_BAUD);
  sprintf(buff, "\nListening at %d Mhz...", FREQUENCY==RF69_433MHZ ? 433 : FREQUENCY==RF69_868MHZ ? 868 : 915);
  Serial.println(buff);
//#ifdef ENABLE_ATC
//  Serial.println("RFM69_ATC Enabled (Auto Transmission Control)");
//#endif
}

void loop()
{
  if (radio.receiveDone())
  {
//    if (promiscuousMode)
//    {
//      Serial.print('[');Serial.print(radio.SENDERID, DEC);Serial.print("] ");
      for (byte i = 0; i < radio.DATALEN; i++) //can also use radio.GetDataLen() if you don't like pointers
        Serial.print((char)radio.DATA[i]);

      if (radio.ACKRequested())
      {
        byte theNodeID = radio.SENDERID;
        radio.sendACK();
        //Serial.print("ACK ok");
      }
//    }
//    else
//      Serial.print("BAD-STUFF");
    
    Serial.println();
    Blink(LED,1);
    //Blink(LED_green,1);
  }
}
void Blink(byte PIN, int DELAY_MS)
{
  pinMode(PIN, OUTPUT);
  digitalWrite(PIN,HIGH);
  delay(DELAY_MS);
  digitalWrite(PIN,LOW);
}
